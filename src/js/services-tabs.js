$(document).ready(function () {
    $('#legal-btn').click(function () {
        $(this).addClass('active-btn');
        $('#private-btn').removeClass('active-btn');

        $('#legal').removeClass('hide-tab').addClass('show-tab');
        $('#private').removeClass('show-tab').addClass('hide-tab');
    });

    $('#private-btn').click(function () {
        $(this).addClass('active-btn');
        $('#legal-btn').removeClass('active-btn');

        $('#private').removeClass('hide-tab').addClass('show-tab');
        $('#legal').removeClass('show-tab').addClass('hide-tab');
    })
});